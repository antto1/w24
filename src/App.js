import React, { useState } from 'react';
import Month from './Month';

function App() {
  const [selectedMonth] = useState(1);

  return (
      <div>      
        <h1>Choose a month</h1>
        <Month selectedMonth={selectedMonth} />
      </div>

  );
}

export default App;