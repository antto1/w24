import React, { useState } from 'react';
import Day from './Day';

const Month = () => {
  const [selectedMonth, setSelectedMonth] = useState(1);

  const handleMonthChange = (e) => {
    setSelectedMonth(Number(e.target.value));
  };

  return (
    <div>
      <label>Select Month: </label>
      <select value={selectedMonth} onChange={handleMonthChange}>
        {Array.from({ length: 12 }, (_, i) => i + 1).map((month) => (
          <option key={month} value={month}>
            {month}
          </option>
        ))}
      </select>

      <Day selectedMonth={selectedMonth} />
    </div>
  );
};

export default Month;