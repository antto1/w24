import React from 'react';

const Day = ({ selectedMonth }) => {
  const getStartDay = (month, year) => {
    const firstDay = new Date(`${year}-${month}-01`);
    return firstDay.getDay();
  };

  const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const startingDay = getStartDay(selectedMonth, 2021);

  return (
    <div>
      {`Month ${selectedMonth}/2021 starts ${daysOfWeek[startingDay]}`}
    </div>
  );
};

export default Day;